from django.urls import path
from todos.views import todo_list_view, todo_list_detail, create_todo_list

urlpatterns = [
    path("", create_todo_list, name="todo_list_create"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list_view, name="todo_list_list"),
]
