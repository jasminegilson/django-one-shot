from asyncio import tasks
from django.shortcuts import redirect, render, get_object_or_404
from .models import TodoList
from .forms import TodoListForm

# Create your views here.
def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
        }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
     #  "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list")

    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)
